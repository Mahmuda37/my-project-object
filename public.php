<?php
class Person
{
	public $name="Mahmuda";

	public function getName()
	{
		echo "My name is Mahmuda Akter<br>";
	}

	public function name()
	{
		$this->getName(); //We can another method by this method
	}
}

class AnotherPerson
{
	public function yourName()
	{
		$obj=new Person;
		$obj->name();//call 2:Second a person class ar 
	} 
}

//$obj=new Person;
$obj=new AnotherPerson;
$obj->yourName();	///call 1:First a AnotherPeron ar function ta k call lorlam